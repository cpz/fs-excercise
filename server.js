const bodyParser = require('body-parser')
const express = require('express')
const multer = require('multer');
const fs = require('fs');
const sharp = require('sharp')
const List = require("./listModel");

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true}))
app.use(express.static('static'));
app.use(express.static('uploads'));

app.get('/list', (req, res) => {
  List.find({}, null, { sort: { order: -1 }}, function(err, itemList) {
    res.send(itemList)
  });
});
app.put('/item', (req, res) => {
  item = req.body;
  item.image = item.image.slice(0, 25);
  item.description = item.description.slice(0, 300);
  let list = new List(item);
  list.save().then( result => {
    res.send(result)
  }).catch(err => {
    res.status(400).send("Unable to save data");
  });
})
app.post('/item/:id', (req, res) => {
  item = req.body;
  item.image = item.image.slice(0, 25);
  item.description = item.description.slice(0, 300);
  // let list = new List(item);
  List.update({ _id: req.params.id }, item).then( result => {
    res.send(result)
  }).catch(err => {
    res.status(400).send("Unable to save data");
  });
})
app.delete('/item/:id', (req, res) => {
  List.find({ _id: req.params.id }).remove().then( result => {
    res.send('"ok"')
  }).catch(err => {
    res.status(400).send("Unable to save data");
  });
});
app.post('/sort', (req, res) => {
  order = req.body.reverse();
  order.forEach((e,i) => {
    List.update({ _id: e }, { order: i }, {multi:true}, function(err, num){
      console.log(num)  
    })
  });
  res.send('"ok"')
})
var filename;
const multerConfig = {
  storage: multer.diskStorage({
    destination: function(req, file, next){
      next(null, './uploads');
    },
    filename: function(req, file, next){
      console.log(file);
      const ext = file.mimetype.split('/')[1];
      filename = file.fieldname + '-' + Date.now() + '.'+ext;
      next(null, filename);
    }
  }),
  fileFilter: function(req, file, next){
    if(!file) next();
    const image = file.mimetype.startsWith('image/');
    if(image) {
      console.log('photo uploaded');
      next(null, true);
    } else {
      console.log("file not supported");
      return next();
    }
  }
}
app.post('/upload',multer(multerConfig).single('photo'),function(req,res){
  let file = 'uploads/'+filename
  sharp(file).resize(320, 320).toBuffer(function(err, buffer) {
    fs.writeFile(file, buffer, function(e) {
      res.send(filename);
    });
  });
})
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})
app.listen(3000, function() {
  console.log('listening on 3000')
})