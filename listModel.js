var mongoose = require('mongoose')
var db = mongoose.connect("mongodb://mongo:27017/list")

var listModel = new mongoose.Schema({
    image: String,
    description: String,
    order: Number
})

module.exports = mongoose.model('list', listModel)
