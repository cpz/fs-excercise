
! function(e, t) {
    if ("function" == typeof define && define.amd) define(["module", "exports"], t);
    else if ("undefined" != typeof exports) t(module, exports);
    else {
        var n = {
            exports: {}
        };
        t(n, n.exports), e.autosize = n.exports
    }
}(this, function(e, t) {
    "use strict";
    var n, o, p = "function" == typeof Map ? new Map : (n = [], o = [], {
            has: function(e) {
                return -1 < n.indexOf(e)
            },
            get: function(e) {
                return o[n.indexOf(e)]
            },
            set: function(e, t) {
                -1 === n.indexOf(e) && (n.push(e), o.push(t))
            },
            delete: function(e) {
                var t = n.indexOf(e); - 1 < t && (n.splice(t, 1), o.splice(t, 1))
            }
        }),
        c = function(e) {
            return new Event(e, {
                bubbles: !0
            })
        };
    try {
        new Event("test")
    } catch (e) {
        c = function(e) {
            var t = document.createEvent("Event");
            return t.initEvent(e, !0, !1), t
        }
    }

    function r(r) {
        if (r && r.nodeName && "TEXTAREA" === r.nodeName && !p.has(r)) {
            var e, n = null,
                o = null,
                i = null,
                d = function() {
                    r.clientWidth !== o && a()
                }, l = function(t) {
                    window.removeEventListener("resize", d, !1), r.removeEventListener("input", a, !1), r.removeEventListener("keyup", a, !1), r.removeEventListener("autosize:destroy", l, !1), r.removeEventListener("autosize:update", a, !1), Object.keys(t).forEach(function(e) {
                        r.style[e] = t[e]
                    }), p.delete(r)
                }.bind(r, {
                    height: r.style.height,
                    resize: r.style.resize,
                    overflowY: r.style.overflowY,
                    overflowX: r.style.overflowX,
                    wordWrap: r.style.wordWrap
                });
            r.addEventListener("autosize:destroy", l, !1), "onpropertychange" in r && "oninput" in r && r.addEventListener("keyup", a, !1), window.addEventListener("resize", d, !1), r.addEventListener("input", a, !1), r.addEventListener("autosize:update", a, !1), r.style.overflowX = "hidden", r.style.wordWrap = "break-word", p.set(r, {
                destroy: l,
                update: a
            }), "vertical" === (e = window.getComputedStyle(r, null)).resize ? r.style.resize = "none" : "both" === e.resize && (r.style.resize = "horizontal"), n = "content-box" === e.boxSizing ? -(parseFloat(e.paddingTop) + parseFloat(e.paddingBottom)) : parseFloat(e.borderTopWidth) + parseFloat(e.borderBottomWidth), isNaN(n) && (n = 0), a()
        }

        function s(e) {
            var t = r.style.width;
            r.style.width = "0px", r.offsetWidth, r.style.width = t, r.style.overflowY = e
        }

        function u() {
            if (0 !== r.scrollHeight) {
                var e = function(e) {
                    for (var t = []; e && e.parentNode && e.parentNode instanceof Element;) e.parentNode.scrollTop && t.push({
                        node: e.parentNode,
                        scrollTop: e.parentNode.scrollTop
                    }), e = e.parentNode;
                    return t
                }(r),
                    t = document.documentElement && document.documentElement.scrollTop;
                r.style.height = "", r.style.height = r.scrollHeight + n + "px", o = r.clientWidth, e.forEach(function(e) {
                    e.node.scrollTop = e.scrollTop
                }), t && (document.documentElement.scrollTop = t)
            }
        }

        function a() {
            u();
            var e = Math.round(parseFloat(r.style.height)),
                t = window.getComputedStyle(r, null),
                n = "content-box" === t.boxSizing ? Math.round(parseFloat(t.height)) : r.offsetHeight;
            if (n !== e ? "hidden" === t.overflowY && (s("scroll"), u(), n = "content-box" === t.boxSizing ? Math.round(parseFloat(window.getComputedStyle(r, null).height)) : r.offsetHeight) : "hidden" !== t.overflowY && (s("hidden"), u(), n = "content-box" === t.boxSizing ? Math.round(parseFloat(window.getComputedStyle(r, null).height)) : r.offsetHeight), i !== n) {
                i = n;
                var o = c("autosize:resized");
                try {
                    r.dispatchEvent(o)
                } catch (e) {}
            }
        }
    }

    function i(e) {
        var t = p.get(e);
        t && t.destroy()
    }

    function d(e) {
        var t = p.get(e);
        t && t.update()
    }
    var l = null;
    "undefined" == typeof window || "function" != typeof window.getComputedStyle ? ((l = function(e) {
        return e
    }).destroy = function(e) {
        return e
    }, l.update = function(e) {
        return e
    }) : ((l = function(e, t) {
        return e && Array.prototype.forEach.call(e.length ? e : [e], function(e) {
            return r(e)
        }), e
    }).destroy = function(e) {
        return e && Array.prototype.forEach.call(e.length ? e : [e], i), e
    }, l.update = function(e) {
        return e && Array.prototype.forEach.call(e.length ? e : [e], d), e
    }), t.
    default = l, e.exports = t.
    default
});
! function(e, t) {
    ! function(e, t) {
        "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? module.exports = t() : e.StickyHeaders = t()
    }(this, function() {
        "use strict";

        function i(e, t) {
            this.element = e, this.options = t, this.stuckHeadersHeight = 0, this._updating = !1, this._events = [], this._readListStyles(), this.headers = Array.prototype.map.call(e.querySelectorAll(this.options.headerSelector), function(e, t) {
                var i = e.getBoundingClientRect(),
                    n = e.cloneNode(!0);
                return n.classList.add("sticky-header", "is-stuck"), n.style.height = i.height + "px", this.headerContainerHeight = i.height, n.dataset.index = t, {
                    top: i.top - this._listStyles.top,
                    height: i.height,
                    el: n
                }
            }.bind(this)), this._createHeaderContainer(), this._on("scroll", this.onScroll)
        }
        var n = function() {
            var t = e.createElement("div");
            t.className = "js-scrollbar-measure", e.body.appendChild(t);
            var i = t.offsetWidth - t.clientWidth;
            return e.body.removeChild(t), i
        }(),
            s = function() {
                for (var i = ["transform", "webkitTransform"], n = 0; n < i.length; n++)
                    if (e.body.style[i[n]] !== t) return i[n]
            }(),
            r = 50,
            o = 16;
        return i.prototype._readListStyles = function() {
            var e = this.element;
            this._listStyles = {
                top: e.getBoundingClientRect().top,
                borderTopWidth: e.clientTop,
                borderLeftWidth: e.clientLeft
            }
        }, i.prototype._createHeaderContainer = function() {
            var t = this.header = e.createElement("div");
            t.className = "sticky-container";
            var i = e.createElement("div"),
                s = this._listStyles.borderLeftWidth;
            i.className = "sticky-container-inner", i.style.top = this._listStyles.borderTopWidth + "px", i.style.left = s + "px", i.style.right = n + s + "px", i.style.height = this.headerContainerHeight + "px", t.appendChild(i);
            var r = this.headerContainer = e.createElement("div");
            i.appendChild(r), t.addEventListener("click", this.onHeaderActivate.bind(this)), t.addEventListener("wheel", this.onHeaderScroll.bind(this)), this.element.parentNode.insertBefore(t, this.element)
        }, i.prototype.onHeaderActivate = function(e) {
            if (e.target.classList.contains("is-stuck")) {
                var t = parseInt(e.target.dataset.index, 10),
                    i = this.headers[t];
                this.element.scrollTop = i.top
            }
        }, i.prototype.onScroll = function() {
            this._latestKnownScrollTop = this.element.scrollTop, this._requestUpdate()
        }, i.prototype._requestUpdate = function() {
            this._updating || (setTimeout(this.updateHeaders.bind(this), 0), this._updating = !0)
        }, i.prototype.updateHeaders = function() {
            var e = this._latestKnownScrollTop + this._listStyles.borderTopWidth,
                t = 0;
            this.headers.forEach(function(i) {
                i.el.parentNode ? i.top >= e && (this.headerContainer.removeChild(i.el), this.stuckHeadersHeight -= i.height) : i.top < e && (this.headerContainer.appendChild(i.el), this.stuckHeadersHeight += i.height), this.isWithinHeaderContainer(i, e) && (t = i.top - e - this.headerContainerHeight)
            }, this), t += this.headerContainerHeight - this.stuckHeadersHeight, requestAnimationFrame(function(e) {
                this.headerContainer.style[s] = "translateY(" + e + "px)", this._updating = !1
            }.bind(this, t))
        }, i.prototype.onHeaderScroll = function(e) {
            var t = 0;
            switch (e.deltaMode) {
                case WheelEvent.DOM_DELTA_PIXEL:
                    t = e.deltaY;
                    break;
                case WheelEvent.DOM_DELTA_LINE:
                    t = o * e.deltaY;
                    break;
                default:
                    t = r
            }
            this.element.scrollTop += t, e.preventDefault()
        }, i.prototype._on = function(e, t) {
            t = t.bind(this), this.element.addEventListener(e, t), this._events.push({
                el: this.element,
                ev: e,
                handler: t
            })
        }, i.prototype.destroy = function() {
            this._events.forEach(function(e) {
                e.el.removeEventListener(e.ev, e.handler)
            }), this.element.parentNode.removeChild(this.header)
        }, i.prototype.isWithinHeaderContainer = function(e, t) {
            return e.top >= t && e.top <= this.headerContainerHeight + t
        }, i
    })
}(window.document);